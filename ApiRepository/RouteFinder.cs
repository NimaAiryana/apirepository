namespace ApiRepository
{
    public class RouteFinder : IRouteFinder
    {
        public string FindEntityFromUrlPath(string urlPath)
        {
            var paths = urlPath.Split('/');

            if (paths == null || paths.Length < 2 || paths[1] != "api" ) return null;

            // todo change first char to upper case

            return paths[2];
        }
    }
}