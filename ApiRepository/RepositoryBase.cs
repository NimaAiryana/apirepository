using System.Collections.Generic;

namespace ApiRepository
{
    public abstract class RepositoryBase<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : IRepository<TEntity, TKey>, new()
    {
        public string Id { get; set; }

        public bool Delete(TKey id)
        {
            throw new System.NotImplementedException();
        }

        public IList<TEntity> GetEntities()
        {
            return FakeEntity<TEntity, TKey>.GetList();
        }

        public TEntity GetEntity(TKey id)
        {
            throw new System.NotImplementedException();
        }

        public TEntity Insert(TEntity entity)
        {
            throw new System.NotImplementedException();
        }

        public TEntity Modify(TEntity entity)
        {
            throw new System.NotImplementedException();
        }

        public TEntity Replace(TEntity entity)
        {
            throw new System.NotImplementedException();
        }
    }
}