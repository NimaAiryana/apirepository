namespace ApiRepository
{
    public interface IRouteFinder
    {
        string FindEntityFromUrlPath(string urlPath); 
    }
}