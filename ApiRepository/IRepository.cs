using System.Collections.Generic;

namespace ApiRepository
{
    public interface IRepository<TEntity, Tkey>
    {
        string Id { get; set; }
        
        IList<TEntity> GetEntities();

        TEntity GetEntity(Tkey id);

        TEntity Insert(TEntity entity);

        TEntity Replace(TEntity entity);

        TEntity Modify(TEntity entity);

        bool Delete(Tkey id);
    }
}