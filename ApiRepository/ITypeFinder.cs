using System;
using System.Reflection;

namespace ApiRepository
{
    public interface ITypeFinder
    {
        Type FindType(string entityName, Assembly assembly, string namespaceName);

        Type FindTypeFromUrlPath(string urlPath, Assembly assembly, string namespaceName);
    }
}