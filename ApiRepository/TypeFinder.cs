using System;
using System.Reflection;

namespace ApiRepository
{
    public class TypeFinder : ITypeFinder
    {
        public Type FindType(string entityName, Assembly assembly, string namespaceName) => assembly.GetType($"{namespaceName}.{entityName}");

        public Type FindTypeFromUrlPath(string urlPath, Assembly assembly, string namespaceName)
        {
            var findEntityName = new RouteFinder().FindEntityFromUrlPath(urlPath);

            if (findEntityName == null) return null;

            return FindType(findEntityName, assembly, namespaceName);
        }
    }
}