using System.Collections.Generic;

namespace ApiRepository
{
    public class FakeEntity<TEntity, TKey> where TEntity : IRepository<TEntity, TKey>, new()
    {
        public static IList<TEntity> GetList()
        {
            return new List<TEntity>() {
                new TEntity() {
                    Id = "0"
                },
                new TEntity() {
                    Id = "1"
                }
            };
        }



        
    }
}