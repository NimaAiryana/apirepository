using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities
{
    public abstract class Document : IDocument
    {
        public ObjectId Id { get; set; }

        public string StringId => Id.ToString();

        public DateTime CreatedAt => Id.CreationTime;

        [BsonIgnore]
        public string IdValue { get; set; }
    }
}