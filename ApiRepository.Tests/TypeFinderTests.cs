using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ApiRepository.Tests
{
    [TestClass]
    public class TypeFinderTests
    {
        [TestMethod]
        public void FindTypeFromUrlPathTest__Set_Api_Entity__ShouldReturnEntityType()
        {
            var path = "/api/Entity";

            var namespaceName = "ApiRepository.Tests";

            var act = new TypeFinder().FindTypeFromUrlPath(path, typeof(ApiRepository.Tests.Entity).Assembly, namespaceName);

            if (act == null) throw new Exception("act is null");

            Assert.AreEqual(typeof(Entity).FullName, act.FullName);
        }
    }
}