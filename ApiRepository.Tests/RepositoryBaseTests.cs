using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace ApiRepository.Tests
{
    [TestClass]
    public class RepositoryBaseTests
    {
        [TestMethod]
        public void GetListTest__SetPath_Api_Entity_AndSetNewInstanceOfTypeWithActivator__ShouldReturnEntityList()
        {
            var path = "/api/Entity";

            var namespaceName = "ApiRepository.Tests";

            var findType = new TypeFinder().FindTypeFromUrlPath(path, typeof(ApiRepository.Tests.Entity).Assembly, namespaceName);

            var instance = Activator.CreateInstance(findType) as IRepository<Entity, string>; // todo problem is here , i have not any class , class should dynamic found

            if (instance == null) throw new Exception("instance is null");
            
            var firstRecord = instance.GetEntities().First();

            Assert.AreEqual("0", firstRecord.Id);
        }
    }
}
