using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ApiRepository.Tests
{
    [TestClass]
    public class RouteFinderTests
    {
        [TestMethod]
        public void FindEntityFromUrlPathTest__SetPath_Api_Entity__ShouldReturnEntity()
        {
            string path = "/api/Entity";

            var acutal = new RouteFinder().FindEntityFromUrlPath(path);

            Assert.AreEqual("Entity", acutal);
        }
    }
}
